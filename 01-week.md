# My submission
After reviewing the different codebooks, I have decides to use some datasets of the Gapminder World web-page.
As stated in the introduction "Alternately, you are welcome to use a data set of your own." [https://www.coursera.org/learn/data-visualization/supplement/xKHAq/course-codebooks] it's possible to use all datasets listed on the Gapminder World web-page.

## STEP 2. Identify a specific topic of interest
I am interested in any relation between BMI and blood pressure.

## STEP 3. Prepare a codebook of your own
I selected the following datasets:

* Body Mass Index (BMI), men, Kg/m2
* Body Mass Index (BMI), women, Kg/m2
* Blood pressure (SBP), men, mmHg
* Blood pressure (SBP), women, mmHg


## STEP 4. Identify a second topic that you would like to explore in terms of its association with your original topic.
Additionally I will compare the  data of sugar intake per person and data to food supply.

Other interesting files might be the "private share of total health spending" or other health spending data.

## STEP 5. Add questions/items/variables documenting this second topic to your personal codebook.
Additional Data will be:

* Sugar per person (g per day)
* Private share of total health spending
* Food supply (kilocalories / person & day)


## STEP 6. Perform a literature review to see what research has been previously done on this topic.

* Use sites such as Google Scholar (http://scholar.google.com)
* Relation of weight and rate of increase in weight during childhood and adolescence to body size, blood pressure, fasting insulin, and lipids in young adults the … [http://circ.ahajournals.org/content/99/11/1471.short]
* Blood pressure is associated with body mass index in both normal and obese children [http://hyper.ahajournals.org/content/36/2/165.short]
* Secular trends in BMI and blood pressure among children and adolescents: the Bogalusa Heart Study [http://pediatrics.aappublications.org/content/130/1/e159.short]


Based on the literature review there should be a correlation between BMI, SBP and Alcohol as well as sugar.
Alcohol consumption is part of other dataset but it seem like it is not included in Gapminder. So a correlation might not be possible in this situation.
I will see if the indicator of food consumption is also related to some of the other data.
