# My submission

## STEPs:

Run frequency distributions for your chosen variables and select columns, and possibly rows.
Your output should be interpretable (i.e. organized and labeled).

## Related to the Task
To tried to identify a difference in the data. I chose from each data set the oldest (1980) and newest (2008) data set.

A normal frequency distributions does not make a lot of sense. I tried to group the data to see how the data is distributed.

There was not filtering of data and no data missing.


# Code

```
import pandas as pd

bmi_female = pd.read_csv('Indicator_BMI_female_ASM_Data.csv', low_memory=False, usecols=["Country", "1980", "2008"])

bmi_male = pd.read_csv('Indicator_BMI_male_ASM_Data.csv', low_memory=False, usecols=["Country", "1980", "2008"]) #

#sbp_female = pd.read_csv('Indicator_SBP_female_ASM_Data.csv', low_memory=False, header=0 , names=["Country", "1980", "2008"], usecols=["SBP female (mm Hg), age standardized mean", "1980", "2008"]) #

sbp_female = pd.read_csv('Indicator_SBP_female_ASM_Data.csv', low_memory=False, usecols=["SBP female (mm Hg), age standardized mean", "1980", "2008"]) #

sbp_male = pd.read_csv('Indicator_SBP_male_ASM_Data.csv', low_memory=False, usecols=["SBP male (mm Hg), age standardized mean", "1980", "2008"]) #

#data['TAB12MDX'] = pd.to_numeric(data['TAB12MDX'], errors='force')

print("\nIndicator BMI female ASM Data")
print("number of observations: ", len(bmi_female))  # number of observations (rows)
print("number of variables: ", len(bmi_female.columns))  # number of variables (columns)
#print(bmi_female)

# frequency distritions
print(bmi_female.describe())

print("\nIndicator SBP female ASM Data")
print("number of observations: ", len(sbp_female))  # number of observations (rows)
print("number of variables: ", len(sbp_female.columns))  # number of variables (columns)
#print(sbp_female)

print(sbp_female.describe())

print("\nIndicator BMI male ASM Data")
print("number of observations: ", len(bmi_male))  # number of observations (rows)
print("number of variables: ", len(bmi_male.columns))  # number of variables (columns)
#print(bmi_male)

print(bmi_male.describe())

print("\nIndicator SBP female ASM Data")
print("number of observations: ", len(sbp_male))  # number of observations (rows)
print("number of variables: ", len(sbp_male.columns))  # number of variables (columns)
#print(sbp_male)

print(sbp_male.describe())

```

# Data


## Table 01

Indicator Body Mass Index (BMI) female ASM Data
number of observations:  199


|         | 1980           |      2008  |
| ------------- |:------:|:------:|
| count | 199.000000 | 199.000000 |
| mean  |  23.551895 |  25.922315 |
| std   |   2.373709 |   2.804152 |
| min   |  18.471660 |  20.545310 |
| 25%   |  21.381535 |  23.725360 |
| 50%   |  23.982930 |  25.991130 |
| 75%   |  25.399470 |  27.485340 |
| max   |  28.279610 |  35.022940 |

## Table 02

Indicator Systolic blood pressure (SBP) female ASM Data
number of observations:  199

|       | 1980           |      2008  |
| ------------- |:------:|:------:|
| count | 199.000000 |  199.000000 |
| mean  | 128.510211 |  126.485436 |
| std   |   5.485456 |    4.352676 |
| min   | 110.335300 |  116.884700 |
| 25%   | 124.992450 |  123.232200 |
| 50%   | 128.745700 |  126.315300 |
| 75%   | 132.224800 |  129.935700 |
| max   | 139.455900 |  136.298900 |

## Table 03

Indicator BMI male ASM Data
number of observations:  199

|       | 1980           |      2008  |
| ------------- |:------:|:------:|
| count |  199.000000 | 199.000000 |
| mean  |   23.154766 |  25.096985 |
| std   |    1.951327 |   2.565742 |
| min   |   19.013940 |  19.866920 |
| 25%   |   21.270820 |  22.832135 |
| 50%   |   23.314240 |  25.498870 |
| 75%   |   24.820280 |  26.822320 |
| max   |   28.124490 |  33.896340 |

## Table 04

Indicator SBP female ASM Data
number of observations:  199

|       | 1980           |      2008  |
| ------------- |:------:|:------:|
| count |  199.000000 |  199.000000 |
| mean  |  132.617067 |  131.180503 |
| std   |    4.898816 |    3.577264 |
| min   |  118.653200 |  122.550500 |
| 25%   |  129.733000 |  128.508600 |
| 50%   |  132.947500 |  131.360000 |
| 75%   |  136.518900 |  133.828950 |
| max   |  143.123700 |  139.417400 |

# Conclusion
I assumed the Body Mass Index (BMI) was increasing over the years.
For the quick comparison between 1980 ans 2008 this was right.
In the result one can see a difference between female and male.

Interesting is also the decrease of the max Systolic blood pressure (SBP)

It will be interesting to see it plotted over time.
