## During my reviews I got the feedback to use some kind of blog.

My answer to this was / is, I do not want to learn an other blog platform. Next to the content to the class I want to learn and improve my git knowledge. Which is an impotent part for data scientist or people working at companies.
Reports, code and documentation can be easily saved and versioned.
At other places I see ipython notebookes with the functionality of versioning,... and so on.

I understand comments like this
> While I understand your desire to use github you can still use a blog to complete these assignments.  I haven't deducted any marks because you didn't but it makes your submissions harder to work with so others might simply because it isn't their job to make up for your choices.

So instead of switching to tumbler I will try to make is easier for a reviewer and generate this overview page and link to the different tasks of each week.

# TOC

## [ Week 01 ] (01-week.md)

## [ Week 02 ] (02-week.md)

## [ Week 03 ] (03-week.md)

## [ Week 04 ] (04-week.md)


# Good Links
good tutorials for pandas:
http://pandas.pydata.org/pandas-docs/stable/tutorials.html
