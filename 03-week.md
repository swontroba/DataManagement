#  My submission

Instead of a blog  or tumbler I use gitlab and markdown which gives me the opportunity to version the file and also learn the work-flow of git in general.

## Not using a blog
I got less point for using not a blog but I do not agree with it. A explanation why I do not use tumblr.
I personally think its more important to learn git and markdown instead of an other blog platform.

## Frequency distributions
Last time I wrote, it does not make sense to apply a frequency distributions to the data-sets I use.

I did not mean to say it does not make sense in general but in my case numbers of Body Mass Index (BMI) and Systolic blood pressure (SBP) are unique.
So compared to the demo / video I would get a long list of number.

I agree the information that each value is unique is also important.

Unique values: bmi female Series([], Name: 1980, dtype: int64)
Unique values: bmi male Series([], Name: 1980, dtype: int64)
Unique values: sbp female Series([], Name: 1980, dtype: int64)
Unique values: spb male Series([], Name: 1980, dtype: int64)

# Data sets
The BMI and SBP data is available for all countries from 1980 till 2008

Sugar consumption from 1961 till 2004

Food consumption from 1966 till 2007

The Data private share of total health spending is available from 1995 till 2010

Data of the last three datasets is limited to specific countries

|    what     | start |  end  |
| ------------- |:------:|:------:|
| sugar cons        | 1961 | 2004 |
| food cons         | 1966 | 2007 |
| priv share health  | 1995 | 2010 |
| bmi and sbp data  | 1980 | 2008 |


|  country      | 1961  |  1962  |  1963  |  1964   | 1965  |  1966  |
| ------------- | ----- | -------| ------ | ------- | ----- | ------ |
|    Abkhazia   |  NaN  |   NaN  |   NaN  |   NaN   |  NaN  |   NaN  |
| Afghanistan   |  NaN  |   NaN  |   NaN  |   NaN   |  NaN  |   NaN  |

## Decisions about data management

To see a relationship between data and changes I will select only the countries where I have the information / value available for all datasets.
I will select the earliest and latest value possible to see if there is an increase o any other changes.
For some plots / curves the whole series might be interesting.

# Distribution tables

## distribution bmi_female 1980 and 2008

1980

|  group           | amount  |
| ---------------- | ----- |
| (18.462, 19.452] |     9 |
| (19.452, 20.433] |    16 |
| (20.433, 21.414] |    27 |
| (21.414, 22.395] |    12 |
| (22.395, 23.376] |    10 |
| (23.376, 24.356] |    41 |
| (24.356, 25.337] |    32 |
| (25.337, 26.318] |    32 |
| (26.318, 27.299] |    16 |
| (27.299, 28.28]  |     4 |

dtype: int64

2008

|  group             | amount  |
| ----------------   | ----- |
| (20.531, 21.993]   |   17 |
| (21.993, 23.441]   |   28 |
| (23.441, 24.889]   |   21 |
| (24.889, 26.336]   |   47 |
| (26.336, 27.784]   |   43 |
| (27.784, 29.232]   |   23 |
| (29.232, 30.68]    |   10 |
| (30.68, 32.127]    |    6 |
| (32.127, 33.575]   |    0 |
| (33.575, 35.0229]  |    4 |
dtype: int64

## distribution bmi male 1980 and 2008

1980

|  group              | amount  |
| ------------------- | ----- |
| (19.00483, 19.925]  |   8  |
| (19.925, 20.836]    |  23  |
| (20.836, 21.747]    |  30  |
| (21.747, 22.658]    |  19  |
| (22.658, 23.569]    |  23  |
| (23.569, 24.48]     |  24  |
| (24.48, 25.391]     |  50  |
| (25.391, 26.302]    |  19  |
| (26.302, 27.213]    |   2  |
| (27.213, 28.124]    |   1  |
dtype: int64

2008

|  group             | amount  |
| ------------------ | ----- |
| (19.853, 21.27]    |  14 |
| (21.27, 22.673]    |  35 |
| (22.673, 24.0757]  |  20 |
| (24.0757, 25.479]  |  30 |
| (25.479, 26.882]   |  52 |
| (26.882, 28.285]   |  36 |
| (28.285, 29.688]   |   6 |
| (29.688, 31.0905]  |   4 |
| (31.0905, 32.493]  |   0 |
| (32.493, 33.896]   |   2 |
dtype: int64

##distribution sbp_female 1980 and 2008

1980

|  group               | amount  |
| -------------------- | ----- |
| (110.306, 113.247]   |   2 |
| (113.247, 116.159]   |   0 |
| (116.159, 119.0715]  |   9 |
| (119.0715, 121.984]  |  10 |
| (121.984, 124.896]   |  28 |
| (124.896, 127.808]   |  39 |
| (127.808, 130.72]    |  42 |
| (130.72, 133.632]    |  31 |
| (133.632, 136.544]   |  26 |
| (136.544, 139.456]   |  12 |
dtype: int64

2008

|  group              | amount  |
| ------------------- | ----- |
| (116.865, 118.826]  |   8 |
| (118.826, 120.768]  |   9 |
| (120.768, 122.709]  |  25 |
| (122.709, 124.65]   |  30 |
| (124.65, 126.592]   |  37 |
| (126.592, 128.533]  |  16 |
| (128.533, 130.475]  |  35 |
| (130.475, 132.416]  |  22 |
| (132.416, 134.357]  |  10 |
| (134.357, 136.299]  |   7 |
dtype: int64

## distribution sbp_male 1980 and 2008

1980

|  group              | amount  |
| ------------------- | ----- |
| (118.629, 121.1]    |   3 |
| (121.1, 123.547]    |   8 |
| (123.547, 125.994]  |   9 |
| (125.994, 128.441]  |  18 |
| (128.441, 130.888]  |  30 |
| (130.888, 133.335]  |  34 |
| (133.335, 135.783]  |  40 |
| (135.783, 138.23]   |  32 |
| (138.23, 140.677]   |  17 |
| (140.677, 143.124]  |   8 |
dtype: int64

2008

|  group              | amount  |
| ------------------- | ----- |
| (122.534, 124.237]  |   7 |
| (124.237, 125.924]  |  11 |
| (125.924, 127.611]  |  18 |
| (127.611, 129.297]  |  27 |
| (129.297, 130.984]  |  28 |
| (130.984, 132.671]  |  31 |
| (132.671, 134.357]  |  39 |
| (134.357, 136.044]  |  21 |
| (136.044, 137.731]  |  14 |
| (137.731, 139.417]  |   3 |
dtype: int64

## distribution sugar consumption 2004

2004

|  group            | amount  |
| ----------------  | ----- |
| (5.294, 24.11]    |  27 |
| (24.11, 42.74]    |  20 |
| (42.74, 61.37]    |   8 |
| (61.37, 8]        |  24 |
| (8, 98.63]        |  33 |
| (98.63, 117.26]   |  20 |
| (117.26, 135.89]  |  22 |
| (135.89, 154.52]  |  10 |
| (154.52, 173.15]  |  10 |
| (173.15, 191.78]  |   1 |
dtype: int64

## distribution food consumption 2004

2004

|  group                | amount  |
| --------------------- | ----- |
| (1540.207, 1766.735]  |   3 |
| (1766.735, 1991.02]   |   8 |
| (1991.02, 2215.305]   |  22 |
| (2215.305, 2439.59]   |  26 |
| (2439.59, 2663.875]   |  21 |
| (2663.875, 2888.16]   |  26 |
| (2888.16, 3112.445]   |  32 |
| (3112.445, 3336.73]   |  13 |
| (3336.73, 3561.015]   |  16 |
| (3561.015, 3785.3]    |   9 |
dtype: int64

Process finished with exit code 0


# Code

```

import pandas as pd

# Todo: filter data to limits
# sugar_cons        1961 - 2004
# food_cons         1966 - 2007
# priv_share_healt  1995 - 2010
# bmi and sbp data  1980 - 2008

bmi_female = pd.read_csv('Indicator_BMI_female_ASM_Data.csv', low_memory=False)
bmi_male = pd.read_csv('Indicator_BMI_male_ASM_Data.csv', low_memory=False)

sbp_female = pd.read_csv('Indicator_SBP_female_ASM_Data.csv', low_memory=False)
sbp_male = pd.read_csv('Indicator_SBP_male_ASM_Data.csv', low_memory=False)

sugar_cons = pd.read_csv("indicator_sugar_consumption_Data.csv", low_memory=False)
food_cons = pd.read_csv("indicator_food_consumption_Data.csv", low_memory=False)
priv_share_health_spend = pd.read_csv("indicator_private_share_of_total_health_spending_Data.csv", low_memory=False)

"""
Print out an overview - data complete?
"""
#print(bmi_female.head(20))
#print(bmi_male.head(20))
#print(sbp_female.head(20))
#print(sbp_male.head(20))
#print(sugar_cons.head(20))
#print(food_cons.head(20))
#print(priv_share_health_spend.head(20))

# Freq distribuiont for BMI and SPB Values
freq_distr = bmi_female["1980"].value_counts(sort=False, normalize=False)
#print(freq_distr)
print("Unique values: bmi female", freq_distr[freq_distr != 1])

freq_distr = bmi_male["1980"].value_counts(sort=False, normalize=False)
#print(freq_distr)
print("Unique values: bmi male", freq_distr[freq_distr != 1])

freq_distr = sbp_female["1980"].value_counts(sort=False, normalize=False)
#print(freq_distr)
print("Unique values: sbp female", freq_distr[freq_distr != 1])

freq_distr = sbp_male["1980"].value_counts(sort=False, normalize=False)
#print(freq_distr)
print("Unique values: spb male", freq_distr[freq_distr != 1])


#usecols=["Country", "1980", "1995", "2004", "2008"]

#bmi_female_bins = pd.qcut(bmi_female["1980"], 10)

def print_distribution_values(my_data_set, my_bins):
    val_bins = pd.cut(my_data_set, my_bins)
    print(pd.groupby(val_bins,[val_bins]).size())


# distribution bmi_female 1980 and 2008
print_distribution_values(bmi_female["1980"], 10)
print_distribution_values(bmi_female["2008"], 10)

# distribution bmi_male 1980 and 2008
print_distribution_values(bmi_male["1980"], 10)
print_distribution_values(bmi_male["2008"], 10)

# distribution sbp_female 1980 and 2008
print_distribution_values(sbp_female["1980"], 10)
print_distribution_values(sbp_female["2008"], 10)

# distribution spb_male 1980 and 2008
print_distribution_values(sbp_male["1980"], 10)
print_distribution_values(sbp_male["2008"], 10)

# distr sugar consumption
print_distribution_values(sugar_cons["2004"], 10)

# distr food consumption
print_distribution_values(food_cons["2004"], 10)

```
