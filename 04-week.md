#  My submission

work in progress ....

[Overview all posts - toc](https://gitlab.com/swontroba/DataManagement/)

So far I did get the distplot to work.
A label for data just worked with kde arguments and not without.

I switched also to an other machine as I had problems with matplotlib in a virtualenv on a mac.

If someone with more python experience in plotting reads this may be he has some hints... the information I found did not work out - well still learning how to apply,..


## Below some chart of my data:
In general data shifted from lower values to upper values.

The hight is not the amount of the value - I mean lowering means they moved to the left.

The hight is based on the amount of countries includes in the bin.

I would like to identify with other plots and plots by country if the movement is related to specific countries

![BMI Female](myfig_bmi_female.png)

![BMI Male](myfig_bmi_male.png)

![SBP Female](myfig_sbp_female.png)

![SBP Male](myfig_sbp_male.png)

![Food consumption](myfig_food_cons.png)

![Sugar consumption](myfig_sug_cons.png)


I still work on my scatter plots... which will get more insights into relations with sugar and food consumption

We notice that there is a positive relation between these two variables in 1980 for me it was interesting to see the change between the to compared years.

![Scatterplot BMI female bmi sbp 1980](myfig_scat-female-bmi-sbp-1980.png)

For the first there is a positiv relationship between both values.

![Scatterplot BMI female bmi sbp 2004](myfig_scat-female-bmi-sbp-2004.png)

The relationship is changing.


![Scatterplot BMI male bmi sbp 1980](myfig_scat-male-bmi-sbp-1980.png)

Similar situation for the maile relation between both values.

![Scatterplot BMI male bmi sbp 2004](myfig_scat-male-bmi-sbp-2004.png)

# Code

```
import pandas as pd
import seaborn
import matplotlib.pyplot as plt



# Todo: filter data to limits
# sugar_cons        1961 - 2004
# food_cons         1966 - 2007
# priv_share_healt  1995 - 2010
# bmi and sbp data  1980 - 2008

bmi_female = pd.read_csv('Indicator_BMI_female_ASM_Data.csv', low_memory=False)
bmi_male = pd.read_csv('Indicator_BMI_male_ASM_Data.csv', low_memory=False)

sbp_female = pd.read_csv('Indicator_SBP_female_ASM_Data.csv', low_memory=False)
sbp_male = pd.read_csv('Indicator_SBP_male_ASM_Data.csv', low_memory=False)

sugar_cons = pd.read_csv("indicator_sugar_consumption_Data.csv", low_memory=False)
food_cons = pd.read_csv("indicator_food_consumption_Data.csv", low_memory=False)
priv_share_health_spend = pd.read_csv("indicator_private_share_of_total_health_spending_Data.csv", low_memory=False)

#setting variables you will be working with to numeric
#bmi_male["1989"] = data['TAB12MDX'].convert_objects(convert_numeric=True)

"""
Print out an overview - data complete?
"""
#print(bmi_female.head(20))
#print(bmi_male.head(20))
#print(sbp_female.head(20))
#print(sbp_male.head(20))
#print(sugar_cons.head(20))
#print(food_cons.head(20))
#print(priv_share_health_spend.head(20))

# Freq distribuiont for BMI and SPB Values
freq_distr = bmi_female["1980"].value_counts(sort=False, normalize=False)
#print(freq_distr)
print("Unique values: bmi female", freq_distr[freq_distr != 1])

freq_distr = bmi_male["1980"].value_counts(sort=False, normalize=False)
#print(freq_distr)
print("Unique values: bmi male", freq_distr[freq_distr != 1])

freq_distr = sbp_female["1980"].value_counts(sort=False, normalize=False)
#print(freq_distr)
print("Unique values: sbp female", freq_distr[freq_distr != 1])

freq_distr = sbp_male["1980"].value_counts(sort=False, normalize=False)
#print(freq_distr)
print("Unique values: spb male", freq_distr[freq_distr != 1])


#usecols=["Country", "1980", "1995", "2004", "2008"]

#bmi_female_bins = pd.qcut(bmi_female["1980"], 10)

def print_distribution_values(my_data_set, my_bins):
    val_bins = pd.cut(my_data_set, my_bins)
    print(pd.groupby(val_bins,[val_bins]).size())


print("\ndistribution bmi_female 1980 and 2008")
print_distribution_values(bmi_female["1980"], 10)
print_distribution_values(bmi_female["2008"], 10)

print("\ndistribution bmi male 1980 and 2008")
print_distribution_values(bmi_male["1980"], 10)
print_distribution_values(bmi_male["2008"], 10)

print("\ndistribution sbp_female 1980 and 2008")
print_distribution_values(sbp_female["1980"], 10)
print_distribution_values(sbp_female["2008"], 10)

print("\ndistribution sbp_male 1980 and 2008")
print_distribution_values(sbp_male["1980"], 10)
print_distribution_values(sbp_male["2008"], 10)

print("\ndistribution sugar consumption 2004")
print_distribution_values(sugar_cons["2004"], 10)

print("\ndistribution food consumption 2004")
print_distribution_values(food_cons["2004"], 10)


# distplot bmi male 1980 and 2008

myfig_bmi_female = seaborn.distplot(bmi_female["1980"], bins=20, kde=True, kde_kws={"label": "Data Year: 1980"} )
myfig_bmi_female = seaborn.distplot(bmi_female["2004"], bins=20, kde=True, kde_kws={"label": "Data Year: 2004"} )

plt.xlabel('BMI Female Value Bins=20')
plt.ylabel("")

plt.title('BMI female change 1980 to 2004 over all countries')

plt.savefig('../myfig_bmi_female.png' )
#plt.show()
plt.clf() # clear figue to draw new plot

#bmi_male["1980"].name = "1980"
seaborn.distplot(sbp_female["1980"], bins=20, kde=True, kde_kws={"label": "Data Year: 1980"} )
seaborn.distplot(sbp_female["2004"], bins=20, kde=True, kde_kws={"label": "Data Year: 2004"} )

plt.xlabel('SBP Female Value Bins=20')
plt.ylabel("")

plt.title('SBP female change 1980 to 2004 over all countries')

plt.savefig('../myfig_sbp_female.png' )
#plt.show()
plt.clf() # clear figue to draw new plot

# distplot bmi male 1980 and 2008

seaborn.distplot(bmi_male["1980"], bins=20, kde=True, kde_kws={"label": "Data Year: 1980"} )
seaborn.distplot(bmi_male["2004"], bins=20, kde=True, kde_kws={"label": "Data Year: 2004"} )

plt.xlabel('BMI Male Value Bins=20')
plt.ylabel("")

plt.title('BMI male change 1980 to 2004 over all countries')

plt.savefig('../myfig_bmi_male.png' )
#plt.show()
plt.clf() # clear figue to draw new plot

seaborn.distplot(sbp_male["1980"], bins=20, kde=True, kde_kws={"label": "Data Year: 1980"} )
seaborn.distplot(sbp_male["2004"], bins=20, kde=True, kde_kws={"label": "Data Year: 2004"} )

plt.xlabel('SBP Male Value Bins=20')
plt.ylabel("")

plt.title('SBP male change 1980 to 2004 over all countries')

plt.savefig('../myfig_sbp_male.png' )
#plt.show()

plt.clf() # clear figue to draw new plot

# sugar consumptoin
seaborn.distplot(sugar_cons["1980"].dropna(), bins=20, kde=True, kde_kws={"label": "Data Year: 1980"} )
seaborn.distplot(sugar_cons["2004"].dropna(), bins=20, kde=True, kde_kws={"label": "Data Year: 2004"} )

plt.xlabel('Sugar Consumption Value Bins=20')

plt.title('Sugar consumption change 1980 to 2004 over all countries')

plt.savefig('../myfig_sug_cons.png' )
#plt.show()

plt.clf() # clear figue to draw new plot

# food consumptoin
seaborn.distplot(food_cons["1980"].dropna(), bins=20, kde=True, kde_kws={"label": "Data Year: 1980"} )
seaborn.distplot(food_cons["2004"].dropna(), bins=20, kde=True, kde_kws={"label": "Data Year: 2004"} )

plt.xlabel('Food Consumption Value Bins=20')
plt.ylabel("")

plt.title('Food consumption change 1980 to 2004 over all countries')

plt.savefig('../myfig_food_cons.png' )
#plt.show()

plt.clf() # clear figue to draw

data = {}
data["BMI-Male-1980"] = bmi_male["1980"].copy()
data["SBP-Male-1980"] = sbp_male["1980"].copy()
data["BMI-Female-1980"] = bmi_female["1980"].copy()
data["SBP-Female-1980"] = sbp_female["1980"].copy()
#2004
data["BMI-Male-2004"] = bmi_male["2004"].copy()
data["SBP-Male-2004"] = sbp_male["2004"].copy()
data["BMI-Female-2004"] = bmi_female["2004"].copy()
data["SBP-Female-2004"] = sbp_female["2004"].copy()

data["Sug-Cons-1980"] = sugar_cons["1980"].dropna()
data["Sug-Cons-2004"] = sugar_cons["2004"].dropna()


scat1 = seaborn.regplot(x="BMI-Male-1980", y="SBP-Male-1980", fit_reg=True, data=data)
plt.xlabel('BMI Male 1980')
plt.ylabel('SBP Male 1980')
plt.title('Scatterplot for the Association Between Male BMI Rate 1980 and SBP Male 1980')
plt.savefig('../myfig_scat-male-bmi-sbp-1980.png' )
#plt.show()
plt.clf() # clear figue to draw new plot

scat1 = seaborn.regplot(x="BMI-Male-2004", y="SBP-Male-2004", fit_reg=True, data=data)
plt.xlabel('BMI Male 2004')
plt.ylabel('SBP Male 2004')
plt.title('Scatterplot for the Association Between Male BMI Rate 2004 and SBP Male 2004')
plt.savefig('../myfig_scat-male-bmi-sbp-2004.png' )
#plt.show()
plt.clf() # clear figue to draw new plot

scat1 = seaborn.regplot(x="BMI-Female-1980", y="SBP-Female-1980", fit_reg=True, data=data)
plt.xlabel('BMI Female 1980')
plt.ylabel('SBP Female 1980')
plt.title('Scatterplot for the Association Between Female BMI Rate 1980 and SBP Female 1980')
plt.savefig('../myfig_scat-female-bmi-sbp-1980.png' )
#plt.show()
plt.clf() # clear figue to draw new plot

scat1 = seaborn.regplot(x="BMI-Female-2004", y="SBP-Female-2004", fit_reg=True, data=data)
plt.xlabel('BMI Female 2004')
plt.ylabel('SBP Female 2004')
plt.title('Scatterplot for the Association Between Female BMI Rate 2004 and SBP Female 2004')
plt.savefig('../myfig_scat-female-bmi-sbp-2004.png' )
#plt.show()
plt.clf() # clear figue to draw new plot

scat1 = seaborn.regplot(x="BMI-Female-1980", y="Sug-Cons-1980", fit_reg=True, data=data)
plt.xlabel('BMI Female 1980')
plt.ylabel("Sug Cons 1980")
plt.title('Scatterplot for the Association Between Female BMI Rate 1980 and Sugar Consumption 1980')
plt.savefig('../myfig_scat-female-bmi-sug-cons-1980.png' )
#plt.show()
plt.clf() # clear figue to draw new plot

scat1 = seaborn.regplot(x="BMI-Female-2004", y="Sug-Cons-2004", fit_reg=True, data=data)
plt.xlabel('BMI Female 2004')
plt.ylabel("Sug-Cons-2004")
plt.title('Scatterplot for the Association Between Female BMI Rate 2004 and Sugar Consumption 2004')
plt.savefig('../myfig_scat-female-bmi-sug-cons-2004.png' )
#plt.show()
plt.clf() # clear figue to draw new plot

scat1 = seaborn.regplot(x="BMI-Male-1980", y="Sug-Cons-1980", fit_reg=True, data=data)
plt.xlabel('BMI Male 1980')
plt.ylabel("Sug Cons 1980")
plt.title('Scatterplot for the Association Between Male BMI Rate 1980 and Sugar Consumption 1980')
plt.savefig('../myfig_scat-male-bmi-sug-cons-1980.png' )
#plt.show()
plt.clf() # clear figue to draw new plot

scat1 = seaborn.regplot(x="BMI-Male-2004", y="Sug-Cons-2004", fit_reg=True, data=data)
plt.xlabel('BMI Male 2004')
plt.ylabel("Sug-Cons-2004")
plt.title('Scatterplot for the Association Between Male BMI Rate 2004 and Sugar Consumption 2004')
plt.savefig('../myfig_scat-male-bmi-sug-cons-2004.png' )
#plt.show()
plt.clf() # clear figue to draw new plot

```
